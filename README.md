# mgit - handle multiple PDP repos from the same location

== HELP ==  
(first add alias mgit='./mgit.sh' to ~/.bash_profile)  
mgit help  
mgit list => List all purchmedia repos  
mgit run PATH_TO_REPO GIT_COMMAND  
mgit runall GIT_COMMAND  
  
_Examples_  
mgit run ./vendor/purchmedia/news-bundle/ git status  
mgit runall git status