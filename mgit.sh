#!/usr/bin/env bash

LIST_REPOS="$(find . -name '*.git' |grep purchmedia)"
red=`tput setaf 1`
green=`tput setaf 2`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
reset=`tput sgr0`

displayCommand() {
    if hash figlet 2>/dev/null; then
        figlet -c $@
    else
        echo  "Install figlet for a different user display; 'brew install figlet'"
    fi

}

list() {

for repo in ${LIST_REPOS}; do
  echo $repo
done
}

help() {
  echo "${green}== HELP ==${reset}"
  echo "(first add alias mgit='./mgit.sh' to ~/.bash_profile)"
  echo "mgit help"
  echo "mgit list => List all purchmedia repos"
  echo "mgit run <PATH_TO_REPO> <GIT_COMMAND>"
  echo "mgit runall <GIT_COMMAND>"
  echo "mgit newfeature <FEATURE_NAME>"

  echo -e "\n${blue}_Examples_${reset}"
  echo "mgit run ./vendor/purchmedia/news-bundle/ git status"
  echo "mgit runall git status"
}

run() {
  displayCommand ${@:2}
  path=`echo $1 | sed 's/.git//g'`
  cd $path
  ${@:2}
  cd -
}

runall() {
  displayCommand $@

echo -e "\n ${cyan}==>> `pwd` <<==${reset}"
$@

for repo in ${LIST_REPOS}; do
  repoPath=`echo $repo | sed 's/.git//g'`
  echo -e "\n${green}==>> $repoPath <<==${reset}"
  cd $repoPath
  $@
  cd -
done
}

newfeature() {
  echo "not implemented yet"
  echo "ex: checkout -b feature-BTS-9999 et ensuite qui update composer.json et tout ce qu'il faut pour travailler desssus"
}

if [[ $# -eq 0 ]] ; then
    help
fi


# call arguments verbatim:
$@
